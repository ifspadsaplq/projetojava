<%@page import="usuario.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title> Sistema de Hotelaria </title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<style type="text/css">@import url("content/css/main.css"); </style>
	<style type="text/css">@import url("content/vendor/bootstrap/css/bootstrap.min.css");</style>
	<style type="text/css">@import url("content/fonts/font-awesome-4.7.0/css/font-awesome.min.css");</style>
	<style type="text/css">@import url("content/fonts/iconic/css/material-design-iconic-font.min.css");</style>
	<style type="text/css">@import url("content/vendor/bootstrap/css/bootstrap.min.css");</style>
	<style type="text/css">@import url("content/vendor/animate/animate.css"); </style>
	<style type="text/css">@import url("content/vendor/css-hamburgers/hamburgers.min.css");</style>
	<style type="text/css">@import url("content/vendor/animsition/css/animsition.min.css");</style>
	<style type="text/css">@import url("content/vendor/select2/select2.min.css");</style>
	<style type="text/css">@import url("content/vendor/daterangepicker/daterangepicker.css");</style>
	<style type="text/css">@import url("content/css/util.css");</style>
	<style type="text/css">@import url("content/css/main.css");</style>
	<style type="image/png">@import url("content/images/icons/favicon.ico");</style>

</head>

<body>
<%
	if (request.getParameter("user") != null && (request.getParameter("senha") != null )){
		String user = request.getParameter("user").toString();
		String senha= request.getParameter("senha").toString();
		Usuario usuario = new Usuario("" ,user ,senha , "" );
		if ( usuario.checkLogin() ){
			HttpSession httpSession = request.getSession(true);
			httpSession.setAttribute("user", usuario.getLogin());
			response.sendRedirect("Home.jsp");
		}else{
			out.print("Usuario ou Senha inválidos!");
%>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('content/images/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="Login.jsp" method="get">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="user" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="senha" placeholder="senha">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="alert alert-danger" role="alert">
  						<label style="text-align:center"> Login ou senha inválidos </label>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" onclick="submit();">
							Login
						</button>
					</div>

					<div class="text-center p-t-90">
						<a class="txt1" href="EsqueceuSenha.jsp">
							Esqueceu a senha?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>

<%
	HttpSession httpSession = request.getSession(true);
	httpSession.setAttribute("user", "none");
	}
}
	else{
		%>
		<div class="limiter">
		<div class="container-login100" style="background-image: url('content/images/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="Login.jsp" method="get">
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="user" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="senha" placeholder="senha">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" onclick="submit();">
							Login
						</button>
					</div>

					<div class="text-center p-t-90">
						<a class="txt1" href="EsqueceuSenha.jsp">
							Esqueceu a senha?
						</a>
					</div>
				</form>
			</div>
		</div>
		</div>

		<div id="dropDownSelect1"></div>

		<%
	}

%>

	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>

</body>
</html>